import { Router, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';

@Injectable()
export class CheckLoginGuard implements CanActivate {
    constructor(private router: Router, private userService: UserService) { }
    canActivate() {
        if (this.userService.IsLoggedIn()) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}