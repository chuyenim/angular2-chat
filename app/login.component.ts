import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
    selector: 'login-component',
    templateUrl: 'app/login.component.html'
})
export class LoginComponent {
    public password: string = '1234';
    
    constructor(private router: Router, private userService: UserService) { }

    CheckLogin(values: any) {
        this.userService.GetList().subscribe((response: any) => {
            for (var i = 0; i < response.length; i++) {
                if (response[i].name === values.username && response[i].password === values.password) {
                    this.userService.SetLogin(true);
                    this.userService.currentFrom = values.username;
                    this.router.navigate(['/']);
                }
            }

            if (this.userService.IsLoggedIn()) {
                this.userService.SetLogin(true);
                this.router.navigate(['/']);
            } else {
                alert('Invalid username or password!');
            }
        }, error => {
            console.log('System error api');
        });
    }
}