import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html'
})
export class AppComponent implements OnInit {
  public isLoggedIn: boolean;
  public currentName: string;

  constructor(private router: Router, private userService: UserService) {
    this.router.events.subscribe((event) => {
      console.log('route changed');
      this.isLoggedIn = this.userService.IsLoggedIn();
      this.currentName = this.userService.currentFrom;
    });
  }

  ngOnInit() {
    this.isLoggedIn = this.userService.IsLoggedIn();
  }

  Logout() {
    this.isLoggedIn = false;
    this.userService.SetLogin(false);
    this.router.navigate(['/login']);
  }
}
