import { Routes, RouterModule } from '@angular/router';
import { MessageListComponent } from './message.component';
import { LoginComponent } from './login.component';
import { CheckLoginGuard } from './guards/check-login.guard';

const routing: Routes = [
    {path: '', component: MessageListComponent, canActivate: [CheckLoginGuard]},
    {path: 'login', component: LoginComponent}
]
export const appRoutes = RouterModule.forRoot(routing);