import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { MessageService } from './services/message.service';
import { UserService } from './services/user.service';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/Rx';

@Component({
    selector: 'message-list',
    templateUrl: 'app/message.component.html'
})
export class MessageListComponent implements OnInit, OnDestroy {
    public messages: any[];
    public users: any[];
    public from: string;
    public to: string;
    public message_content: string;
    private ioUrl = 'http://localhost:4000/';
    private socket;

    constructor(
        private messageService: MessageService,
        private userService: UserService,
        private elementRef: ElementRef,
        private activatedRoute: ActivatedRoute
    ) {
        this.socket = io(this.ioUrl);
        // this.socket.on('chat message', (data) => {
        //     console.log(data);
        // });
        let listener = Observable.fromEvent(this.socket, 'chat message');
        listener.subscribe((payload: MessageService) => {
            if (this.to && this.from && (
                (payload.from === this.from && payload.to === this.to) ||
                (payload.from === this.to && payload.to === this.from)
            )) {
                this.messages.push(payload);
            }

            // Scroll to bottom of chatbox
            var box = elementRef.nativeElement.querySelectorAll("#messageBox");
            setTimeout(function () {
                box[0].scrollTop = 10E6;
            }, 500);
        });
    }

    ngOnInit() {
        // this.from = 'from 1';
        this.from = this.userService.currentFrom || ''; console.log('user', this.from);
        this.getUserList();
        this.getMessageList();

    }

    changeTo(name: string): void {
        this.to = name;
        this.getMessageList();
    }

    getMessageList() {
        this.messages = [];
        this.messageService.GetList().subscribe((response: any) => {
            for (var i = 0; i < response.length; i++) {
                if (this.to && this.from && (
                    (response[i].from === this.from && response[i].to === this.to) ||
                    (response[i].from === this.to && response[i].to === this.from)
                )) {
                    this.messages.push(response[i]);
                }
            }
        }, error => {
            console.log('System error api');
        });
    }

    getUserList() {
        this.userService.GetList().subscribe((response: any) => {
            this.users = response;
            for (var i = 0; i < this.users.length; i++) {
                if (this.users[i].name === this.from) {
                    this.users.splice(i, 1);
                }
            }
        }, error => {
            console.log('System error api');
        });
    }

    Send(data: any) {
        this.message_content = '';
        data.createdAt = new Date();
        this.socket.emit('chat message', data);
        this.messageService.Add(data).subscribe(response => {
            if (response) {
                //this.getMessageList();
            }
        });
    }

    ngOnDestroy() {
    }
}