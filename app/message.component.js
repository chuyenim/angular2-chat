"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var message_service_1 = require("./services/message.service");
var user_service_1 = require("./services/user.service");
var io = require("socket.io-client");
var Observable_1 = require("rxjs/Observable");
var router_1 = require("@angular/router");
require("rxjs/Rx");
var MessageListComponent = (function () {
    function MessageListComponent(messageService, userService, elementRef, activatedRoute) {
        var _this = this;
        this.messageService = messageService;
        this.userService = userService;
        this.elementRef = elementRef;
        this.activatedRoute = activatedRoute;
        this.ioUrl = 'http://localhost:4000/';
        this.socket = io(this.ioUrl);
        // this.socket.on('chat message', (data) => {
        //     console.log(data);
        // });
        var listener = Observable_1.Observable.fromEvent(this.socket, 'chat message');
        listener.subscribe(function (payload) {
            if (_this.to && _this.from && ((payload.from === _this.from && payload.to === _this.to) ||
                (payload.from === _this.to && payload.to === _this.from))) {
                _this.messages.push(payload);
            }
            // Scroll to bottom of chatbox
            var box = elementRef.nativeElement.querySelectorAll("#messageBox");
            setTimeout(function () {
                box[0].scrollTop = 10E6;
            }, 500);
        });
    }
    MessageListComponent.prototype.ngOnInit = function () {
        // this.from = 'from 1';
        this.from = this.userService.currentFrom || '';
        console.log('user', this.from);
        this.getUserList();
        this.getMessageList();
    };
    MessageListComponent.prototype.changeTo = function (name) {
        this.to = name;
        this.getMessageList();
    };
    MessageListComponent.prototype.getMessageList = function () {
        var _this = this;
        this.messages = [];
        this.messageService.GetList().subscribe(function (response) {
            for (var i = 0; i < response.length; i++) {
                if (_this.to && _this.from && ((response[i].from === _this.from && response[i].to === _this.to) ||
                    (response[i].from === _this.to && response[i].to === _this.from))) {
                    _this.messages.push(response[i]);
                }
            }
        }, function (error) {
            console.log('System error api');
        });
    };
    MessageListComponent.prototype.getUserList = function () {
        var _this = this;
        this.userService.GetList().subscribe(function (response) {
            _this.users = response;
            for (var i = 0; i < _this.users.length; i++) {
                if (_this.users[i].name === _this.from) {
                    _this.users.splice(i, 1);
                }
            }
        }, function (error) {
            console.log('System error api');
        });
    };
    MessageListComponent.prototype.Send = function (data) {
        this.message_content = '';
        data.createdAt = new Date();
        this.socket.emit('chat message', data);
        this.messageService.Add(data).subscribe(function (response) {
            if (response) {
            }
        });
    };
    MessageListComponent.prototype.ngOnDestroy = function () {
    };
    return MessageListComponent;
}());
MessageListComponent = __decorate([
    core_1.Component({
        selector: 'message-list',
        templateUrl: 'app/message.component.html'
    }),
    __metadata("design:paramtypes", [message_service_1.MessageService,
        user_service_1.UserService,
        core_1.ElementRef,
        router_1.ActivatedRoute])
], MessageListComponent);
exports.MessageListComponent = MessageListComponent;
//# sourceMappingURL=message.component.js.map