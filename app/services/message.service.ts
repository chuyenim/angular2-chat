import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class MessageService {
    public from: string;
    public to: string;

    private apiUrl = 'http://586f1e760474f212000b027f.mockapi.io/api/chats/';
    constructor(private _http: Http) {}

    GetList(): Observable<any[]> {
        return this._http.get(this.apiUrl).map((response: Response) => response.json());
    }

    Add(data: any): Observable<any> {
        return this._http.post(this.apiUrl, data).map((response: Response) => response.json());
    }
}
