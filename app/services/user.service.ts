import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    private apiUrl = 'http://586f1e760474f212000b027f.mockapi.io/api/users/';
    public _isLoggedIn: boolean;
    public currentFrom: string;
    public currentTo: string;

    constructor(private _http: Http) { }

    IsLoggedIn() {
        return this._isLoggedIn;
    }

    SetLogin(isLoggedIn: boolean) {
        this._isLoggedIn = isLoggedIn;
    }

    GetList(): Observable<any[]> {
        return this._http.get(this.apiUrl).map((response: Response) => response.json());
    }

    Add(data: any): Observable<any> {
        return this._http.post(this.apiUrl, data).map((response: Response) => response.json());
    }
}
