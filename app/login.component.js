"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var user_service_1 = require("./services/user.service");
var LoginComponent = (function () {
    function LoginComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.password = '1234';
    }
    LoginComponent.prototype.CheckLogin = function (values) {
        var _this = this;
        this.userService.GetList().subscribe(function (response) {
            for (var i = 0; i < response.length; i++) {
                if (response[i].name === values.username && response[i].password === values.password) {
                    _this.userService.SetLogin(true);
                    _this.userService.currentFrom = values.username;
                    _this.router.navigate(['/']);
                }
            }
            if (_this.userService.IsLoggedIn()) {
                _this.userService.SetLogin(true);
                _this.router.navigate(['/']);
            }
            else {
                alert('Invalid username or password!');
            }
        }, function (error) {
            console.log('System error api');
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: 'login-component',
        templateUrl: 'app/login.component.html'
    }),
    __metadata("design:paramtypes", [router_1.Router, user_service_1.UserService])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map